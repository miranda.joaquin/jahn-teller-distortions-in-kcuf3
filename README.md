# Jahn Teller distortions in KCuF3

Coupling of strong correlated electrons to lattice distortions in KCuF3. Effective interatomic interactions are parametrized by ab-initio calculations based on DFT calculation and renormalized by long range Coulomb forces.

## Directories
The folders ewaldconvergen and ewaldirection contain programs to test Ewald convergence and set the direction of the JT modes. The directory disordercell contain several programs that deal only with the ddisplacmentes of single, two-atoms, three atoms of four atoms; they should create a double well energy potential.
