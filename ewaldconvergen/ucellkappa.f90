!    ** REAL-SPACE AND RECIPROCAL-SPACE PARTS OF EWALD SUM FOR IONS.  **
!    **                                                               **
!    ** REFERENCES:                                                   **
!    **                                                               **
!    ** WOODCOCK AND SINGER, TRANS. FARADAY SOC. 67, 12, 1971.        **
!    ** DE LEEUW ET AL., PROC. ROY. SOC. A 373, 27, 1980.             **
!    ** HEYES, J. CHEM. PHYS. 74, 1924, 1981.                         **
!    ** SEE ALSO FINCHAM, MDIONS, CCP5 PROGRAM LIBRARY.               **
!    **                                                               **
!    ** ROUTINES SUPPLIED:                                            **
!    **                                                               **
!    ** SUBROUTINE SETUP ( KAPPA )                                    **
!    **    SETS UP THE WAVEVECTORS FOR USE IN THE EWALD SUM           **
!    ** SUBROUTINE RWALD ( KAPPA, VR )                                **
!    **    CALCULATES THE R-SPACE PART OF THE SUM                     **
!    ** SUBROUTINE KWALD ( KAPPA, VK )                                **
!    **    CALCULATES THE K-SPACE PART OF THE SUM                     **
!    ** REAL FUNCTION ERFC ( X )                                      **
!    **    RETURNS THE COMPLEMENTARY ERROR FUNCTION                   **
!    **                                                               **
!    ** PRINCIPAL VARIABLES:                                          **
!    **                                                               **
!    ** INTEGER  TOTK         THE TOTAL NUMBER OF K-VECTORS STORED    **
!    ** INTEGER  MAXK         MAXIMUM POSSIBLE NUMBER OF K-VECTORS    **
!    ** INTEGER  KMAX         MAX INTEGER COMPONENT OF THE K-VECTOR   **
!    ** INTEGER  KSQMAX       MAX SQUARE MOD OF THE K-VECTOR REQUIRED **
!    ** REAL     VR           ENERGY FROM R-SPACE SUM                 **
!    ** REAL     VK           ENERGY FROM K-SPACE SUM                 **
!    ** REAL     KVEC(MAXK)   ARRAY USED TO STORE K-VECTORS           **
!    ** REAL     KAPPA        WIDTH OF CANCELLING DISTRIBUTION        **
!    **                                                               **
!    ** USAGE:                                                        **
!    **                                                               **
!    ** SETUP IS CALLED ONCE AT THE BEGINNING OF THE SIMULATION       **
!    ** TO CALCULATE ALL THE K-VECTORS REQUIRED IN THE EWALD SUM.     **
!    ** THESE VECTORS ARE USED THROUGHOUT THE SIMULATION IN THE       **
!    ** SUBROUTINE KWALD TO CALCULATE THE K-SPACE CONTRIBUTION TO THE **
!    ** POTENTIAL ENERGY AT EACH CONFIGURATION. THE SELF TERM IS      **
!    ** SUBTRACTED FROM THE K-SPACE CONTRIBUTION IN KWALD.            **
!    ** THE SURFACE TERM FOR SIMULATIONS IN VACUUM IS NOT INCLUDED.   **
!    ** ROUTINE RWALD RETURNS THE R-SPACE CONTRIBUTION TO THE EWALD   **
!    ** SUM AND IS CALLED FOR EACH CONFIGURATION IN THE SIMULATION.   **
!    ** A CUBIC BOX AND UNIT BOX LENGTH ARE ASSUMED THROUGHOUT.       **
!    *******************************************************************

PROGRAM MAIN
       IMPLICIT NONE
       INTEGER :: I, II, J, JJ, N, npart, cell 
       REAL*8  :: KAPPA, L, LRZ, dist, LZ
       REAL*8  :: Energy, VR, VK, En, jtdist
       REAL*8, DIMENSION(:,:), ALLOCATABLE :: DATA
       REAL*8, DIMENSION(:,:), ALLOCATABLE :: POSEN
       REAL*8, DIMENSION(:), ALLOCATABLE :: C
       INTEGER*8, DIMENSION(:), ALLOCATABLE :: GIVENT
       INTEGER :: row, temp
       INTEGER, PARAMETER :: col=7
       REAL,PARAMETER :: convl=0.529177  !Factor to convert Angstrom to Bohr
       ALLOCATE(DATA(row,col))
       ALLOCATE(POSEN(row,col))
       ALLOCATE(C(row*col))
       ALLOCATE(GIVENT(1))
       OPEN (UNIT = 70, FILE = 'data.out',  STATUS = "OLD", ACTION = 'READ')
       OPEN (UNIT = 75, FILE = 'temper.inp',  STATUS = "OLD", ACTION = 'READ')
       OPEN (UNIT = 80, FILE = 'vasp-ewald.out', STATUS = 'OLD', ACTION = 'WRITE')         
       READ(75,*) temp
       READ(*,*) cell, KAPPA

       npart = 20*(cell+1)*(cell+1)*(cell+1)

       SELECT CASE (temp)
         CASE (5)
             LZ=7.8230/convl; L=5.8317/convl; jtdist=0.0193  ! Conversion from Angstrom to Bohr
         CASE (300)
             LZ=7.8491/convl; L=5.8589/convl; jtdist=0.0200 
         CASE (603)
             LZ=7.9004/convl; L=5.9034/convl; jtdist=0.02175;
         CASE (903)
             LZ=7.9558/convl; L=5.9501/convl; jtdist=0.0235
       END SELECT

       LRZ = LZ/L   ! Ratio c/b axis; if LRZ=1 cubic Perovzkite
       JJ = 1
       II = 1
       DO I=1,col
            DO J=1,row
                N=(J+row*(I-1)+6)/col
                POSEN(N,JJ)=DATA(J,I)
                C(II) = POSEN(N,JJ)
                II = II + 1
            END DO
            JJ = JJ + 1
       END DO

      Energy = 0.d0
      VR = 0.d0
      VK = 0.d0
      CALL SETUP(LRZ, KAPPA)
      CALL RWALD(KAPPA, cell, npart, LRZ,  jtdist, VR)
      Energy  =Energy + VR
      CALL KWALD(KAPPA, npart, LRZ, VK)
      Energy = Energy + VK
      Energy = 27.21138386*Energy/((REAL(cell)+1.0)**4)/L    ! Conversion from Hartree to eV
      WRITE(*,*) cell, KAPPA, Energy

      CLOSE(UNIT=70)
      CLOSE(UNIT=75)
      CLOSE(UNIT=80)
      DEALLOCATE (DATA)
      DEALLOCATE (POSEN)
      DEALLOCATE (C)
      DEALLOCATE (GIVENT)
END PROGRAM MAIN

    SUBROUTINE SETUP (LRZ, KAPPA )

        COMMON / BLOCK2 / KVEC

!     *******************************************************************
!     ** ROUTINE TO SET UP THE WAVE-VECTORS FOR THE EWALD SUM.         **
!     **                                                               **
!     ** THE WAVEVECTORS MUST FIT INTO A BOX OF UNIT LENGTH.           **
!     ** IN THIS EXAMPLE WE ALLOW A MAXIMUM OF 1000 WAVEVECTORS.       **
!     *******************************************************************

        INTEGER     MAXK
        PARAMETER ( MAXK = 10000 )

        REAL*8    KVEC(MAXK), KAPPA

        INTEGER    KMAX, KSQMAX, KX, KY, KZ, TOTK
        REAL*8     KSQ 
        REAL*8     TWOPI, B, RKX, RKY, RKZ, RKSQ, LRZ
        PARAMETER ( KMAX = 5, KSQMAX = 50, TWOPI = 6.2831853 )

!     *******************************************************************

        B = 1.0 / 4.0 / KAPPA / KAPPA
!     ** LOOP OVER K-VECTORS. NOTE KX IS NON-NEGATIVE **

        TOTK = 0

        DO 100 KX = 0, KMAX

           RKX = TWOPI * REAL ( KX )

           DO 99 KY = -KMAX, KMAX

              RKY = TWOPI * REAL ( KY )

              DO 98 KZ = -KMAX, KMAX
           
                RKZ = TWOPI * REAL ( KZ ) / LRZ

                 KSQ = KX * KX + KY * KY + KZ * KZ/LRZ/LRZ

                 IF ( ( KSQ .LT. REAL(KSQMAX)) .AND. ( KSQ .NE. 0.d0 ) ) THEN

                    TOTK = TOTK + 1

                    IF ( TOTK .GT. MAXK ) STOP 'KVEC IS TOO SMALL'

                    RKSQ = RKX * RKX + RKY * RKY + RKZ * RKZ
                    KVEC(TOTK) = TWOPI * EXP ( -B * RKSQ ) / RKSQ

                 ENDIF

98            CONTINUE

99         CONTINUE

100     CONTINUE
        
!        WRITE( *, ' ( '' EWALD SUM SETUP COMPLETE ''     ) ' )
!        WRITE( *, ' ( '' NUMBER OF WAVEVECTORS IS '', I5 ) ' ) TOTK

        RETURN
        END

        SUBROUTINE RWALD ( KAPPA, ncell, npart, LRZ, jtdist, VR )
       
        COMMON / BLOCK1 / RX, RY, RZ, Z

!     *******************************************************************
!     ** CALCULATES R-SPACE PART OF POTENTIAL ENERGY BY EWALD METHOD.  **
!     **                                                               **
!     ** PRINCIPAL VARIABLES:                                          **
!     **                                                               **
!     ** INTEGER N                   NUMBER OF IONS                    **
!     ** REAL    RX(N),RY(N),RZ(N)   POSITIONS OF IONS                 **
!     ** REAL    Z(N)                IONIC CHARGES                     **
!     ** REAL    VR                  R-SPACE POTENTIAL ENERGY          **
!     **                                                               **
!     ** dxfn, dyfn, dzfn            DISPLACMENT OF THE FLUORINE       **
!     ** ncu, nk, nf1, nf2           IONIC CHARGES                     **
!     ** L                           LENGTH OF ONE SIDE OF THE BOX     **
!     ** LZ                          LENGTH OF THE Z AXIS BOX          **
!     ** jt                          Amount of Jahn-Teller distortion  ** 
!     ** ROUTINE REFERENCED:                                           **
!     **                                                               **
!     ** REAL FUNCTION ERFC ( X )                                      **
!     **    RETURNS THE COMPLEMENTARY ERROR FUNCTION                   **
!     *******************************************************************

        INTEGER     N, NN, NN1, NN2, I, J, K, ncell, npart, Np, Q
        PARAMETER ( TWOPI = 6.2831853, Np=160000) 
        REAL*8        RX(Np), RY(Np), RZ(Np), Z(Np)
        REAL*8        KAPPA, VR
        REAL*8        L, LRZ, dist, dx13, dy13, dz13, dx15, dy15, dz15
        REAL*8        dx9, dy9, dz9, dx,  dx16, dy16, dz16, jtdist, dist0
        REAL*8        ncu, nk, nf1, nf2 
        REAL*8        RXI, RYI, RZI, ZI, RXIJ, RYIJ, RZIJ
        REAL*8        RIJSQ, RIJ, KRIJ, ERFC, VIJ

!        OPEN (UNIT=90, FILE='crystal.xyz', STATUS='OLD', ACTION='WRITE')
!     *******************************************************************
        N = npart
        VR = 0.d0
        NN = 0
        dist0 = 0.d0 !jtdist  !0.02d0 !0.02175 !0.0235d0
        dx =0.d0; dy=0.d0
        dx13 = 0.d0; dy13 = 0.d0; dz13 = 0.d0
        dx15 = 0.d0; dy15 = 0.d0; dz15 = 0.d0
        dx16 = 0.d0; dy16 = 0.d0; dz16 = 0.d0
        dx9  = 0.d0; dy9  = 0.d0; dz9  = 0.d0
!        Q = 0
        nk =   0.78d0 !1.0d0 
        ncu =  1.55d0 !2.0d0 
        nf1 = -0.75d0 !-1.0d0 
        nf2 = -0.79d0 !-1.0d0 

!               
        DO I= 0, ncell
            DO J= 0, ncell
                DO K= 0, ncell
                        NN=NN+1
                        RX(NN) = REAL(I); RY(NN) = REAL(J); RZ(NN) = LRZ*( 0.25d0 + REAL(K));   Z(NN) =  nk
                        NN=NN+1
                        RX(NN) = REAL(I); RY(NN) = REAL(J); RZ(NN) = LRZ*(-0.25d0 + REAL(K));   Z(NN) =  nk
                        NN=NN+1
                        RX(NN) = 0.5d0 + REAL(I); RY(NN) = 0.5d0 + REAL(J); RZ(NN) = LRZ*(0.25d0 + REAL(K)); Z(NN) =  nk  
                        NN=NN+1
                        RX(NN) = 0.5d0 + REAL(I); RY(NN) = 0.5d0 + REAL(J); RZ(NN) = LRZ*(-0.25d0 + REAL(K)); Z(NN) =  nk
                END DO
            END DO
       END DO
       NN1=NN
       DO I= 0, ncell
           DO J= 0, ncell
               DO K= 0, ncell
                   NN=NN+1
                   RX(NN) = 0.5d0 + REAL(I); RY(NN) =         REAL(J); RZ(NN) = LRZ*REAL(K); Z(NN) =  ncu 
                   NN=NN+1
                   RX(NN) =         REAL(I); RY(NN) = 0.5d0 + REAL(J); RZ(NN) = LRZ*REAL(K); Z(NN) =  ncu
                   NN=NN+1
                   RX(NN) = 0.5d0 + REAL(I); RY(NN) =         REAL(J); RZ(NN) = LRZ*(0.5d0+REAL(K)); Z(NN) =  ncu
                   NN=NN+1
                   RX(NN) =         REAL(I); RY(NN) = 0.5d0 + REAL(J); RZ(NN) = LRZ*(0.5d0+REAL(K)); Z(NN) =  ncu
               END DO
           END DO
       END DO
       NN2=NN
       DO K= 0, ncell
           DO I= 0, ncell
               DO J= 0, ncell
                   NN=NN+1
                   RX(NN) = 0.50d0 + REAL(I); RY(NN) = REAL(J); RZ(NN) = LRZ*(0.25d0 + REAL(K)); Z(NN) =  nf1
                   NN=NN+1
                   RX(NN) = 0.50d0 + REAL(I); RY(NN) = REAL(J); RZ(NN) = LRZ*(-0.25d0 + REAL(K)); Z(NN) =  nf1
                   NN=NN+1
                   RX(NN) =          REAL(I); RY(NN) = 0.5d0 + REAL(J); RZ(NN) = LRZ*(0.25d0 + REAL(K)); Z(NN) =  nf1
                   NN=NN+1
                   RX(NN) =          REAL(I); RY(NN) = 0.5d0 + REAL(J); RZ(NN) = LRZ*(-0.25d0 + REAL(K)); Z(NN) =  nf1
                   NN=NN+1
                   RX(NN) =  0.25d0 + dist0 + dx13 + REAL(I); RY(NN) =  0.25d0 - dist0 + dy13 + REAL(J); RZ(NN) = LRZ*(dz13  + REAL(K)); Z(NN) =  nf2
                   NN=NN+1
                   RX(NN) = -0.25d0 - dist0 + dx   + REAL(I); RY(NN) = -0.25d0 + dist0 + dy   + REAL(J); RZ(NN) = LRZ*(REAL(K)); Z(NN) =  nf2
                   NN=NN+1
                   RX(NN) =  0.25d0 - dist0 + dx15 + REAL(I); RY(NN) = -0.25d0 - dist0 + dy15 + REAL(J); RZ(NN) = LRZ*(dz15 + REAL(K)); Z(NN) =  nf2
                   NN=NN+1
                   RX(NN) = -0.25d0 + dist0 + dx16 + REAL(I); RY(NN) =  0.25d0 + dist0 + dy16 + REAL(J); RZ(NN) = LRZ*(dz16 + REAL(K)); Z(NN) =  nf2
                   NN=NN+1
                   RX(NN) =  0.25d0 + dist0 + dx   + REAL(I); RY(NN) = -0.25d0 + dist0 + dy   + REAL(J); RZ(NN) = LRZ*(0.5d0 + REAL(K)); Z(NN) =  nf2
                   NN=NN+1
                   RX(NN) = -0.25d0 - dist0 + dx   + REAL(I); RY(NN) =  0.25d0 - dist0 + dy   + REAL(J); RZ(NN) = LRZ*(0.5d0 + REAL(K)); Z(NN) =  nf2
                   NN=NN+1
                   RX(NN) =  0.25d0 - dist0 + dx   + REAL(I); RY(NN) =  0.25d0 + dist0 + dy   + REAL(J); RZ(NN) = LRZ*(0.5d0 + REAL(K)); Z(NN) =  nf2
                   NN=NN+1
                   RX(NN) = -0.25d0 + dist0 + dx   + REAL(I); RY(NN) = -0.25d0 - dist0 + dy   + REAL(J); RZ(NN) = LRZ*(0.5d0 + REAL(K)); Z(NN) =  nf2
               END DO
           END DO
       END DO        

!       Routine to write on UNIT=90 positions of ions with format to be ploted with jmol
!       Q: Sum over all chares, Z(I),  to check neutrality

!        WRITE(90,*) N
!        WRITE(90,*) 'KCuF3'
        L=REAL(ncell+1)
        DO I = 1, NN1
!            WRITE(90,'("K", 3F12.4, I4.2)') RX(I),  RY(I), RZ(I), I
            RX(I) = RX(I)/L; RY(I) = RY(I)/L; RZ(I) = RZ(I)/L
!            Q = Q + Z(I)
        END DO
        DO  I = NN1+1, NN2
!            WRITE(90,'("Cu", 3F12.4, I4.2)') RX(I),  RY(I), RZ(I), I
            RX(I) = RX(I)/L; RY(I) = RY(I)/L; RZ(I) = RZ(I)/L
!            Q = Q + Z(I)
        END DO
        DO  I = NN2+1, NN
!            WRITE(90,'("F", 3F12.4, I4.2)') RX(I),  RY(I), RZ(I), I
           RX(I) = RX(I)/L; RY(I) = RY(I)/L; RZ(I) = RZ(I)/L
!            Q = Q + Z(I)
        END DO
!        WRITE(*,*) Q
         
        DO  I = 1, N - 1

           RXI = RX(I)
           RYI = RY(I)
           RZI = RZ(I)
           ZI  = Z(I)

           DO  J = I + 1, N

              RXIJ = RXI - RX(J)
              RYIJ = RYI - RY(J)
              RZIJ = RZI - RZ(J)
              RXIJ = RXIJ - ANINT ( RXIJ )
              RYIJ = RYIJ - ANINT ( RYIJ )
              RZIJ = RZIJ - LRZ*ANINT ( RZIJ )
              RIJSQ = RXIJ * RXIJ + RYIJ * RYIJ + RZIJ * RZIJ
              RIJ   = SQRT ( RIJSQ )
              KRIJ  = KAPPA * RIJ
!              IF(RIJ.GT.1e-8) THEN
              VIJ   = ZI * Z(J) * ERFC ( KRIJ ) / RIJ
!              WRITE(*,'("VRIJ is",F20.10)') VIJ 
              VR    = VR + VIJ
!              END IF

            END DO 

         END DO

!        CLOSE(UNIT=90)
        RETURN
        END


        SUBROUTINE KWALD (KAPPA, npart, LRZ, VK)

        COMMON / BLOCK1 / RX, RY, RZ, Z
        COMMON / BLOCK2 / KVEC

!     *******************************************************************
!     ** CALCULATES K-SPACE PART OF POTENTIAL ENERGY BY EWALD METHOD.  **
!     **                                                               **
!     ** THE SELF TERM IS SUBTRACTED.                                  **
!     ** IN ONE COORDINATE DIRECTION (X), SYMMETRY IS USED TO REDUCE   **
!     ** THE SUM TO INCLUDE ONLY POSITIVE K-VECTORS.                   **
!     ** THE NEGATIVE VECTORS IN THIS DIRECTION ARE INCLUDED BY USE    **
!     ** OF THE MULTIPLICATIVE VARIABLE 'FACTOR'.                      **
!     **                                                               **
!     ** PRINCIPAL VARIABLES:                                          **
!     **                                                               **
!     ** INTEGER N                   NUMBER OF IONS                    **

!     ** REAL    RX(N),RY(N),RZ(N)   POSITIONS OF IONS                 **
!     ** REAL    Z(N)                IONIC CHARGES                     **
!     ** REAL    VK                  K-SPACE POTENTIAL ENERGY          **
!     ** REAL    VKS                 SELF PART OF K-SPACE SUM          **
!     *******************************************************************

        INTEGER     MAXK, npart, N, Np
        PARAMETER ( MAXK = 10000, Np = 160000)
        REAL*8        KVEC(MAXK), RX(Np), RY(Np), RZ(Np), Z(Np)
        REAL*8        KAPPA, VK
        INTEGER     TOTK

        INTEGER     KMAX, KX, KY, KZ, I, KSQMAX
        REAL*8      KSQ
        REAL*8      TWOPI, FACTOR, VD, VS, RSQPI, LRZ
        PARAMETER (KMAX = 5, KSQMAX = 50 )

        PARAMETER ( TWOPI = 6.2831853, RSQPI = 0.5641896 )

        COMPLEX     EIKX(1:Np, 0:KMAX)
        COMPLEX     EIKY(1:Np, -KMAX:KMAX)
        COMPLEX     EIKZ(1:Np, -KMAX:KMAX)
        COMPLEX     EIKR(Np), SUM
        N=npart
!     *******************************************************************

!     ** CONSTRUCT EXP(IK.R) FOR ALL IONS AND K-VECTORS **

!     ** CALCULATE KX, KY, KZ = 0 , -1 AND 1 EXPLICITLY **
          
        DO 10 I = 1, N

           EIKX(I, 0) = (1.0, 0.0)
           EIKY(I, 0) = (1.0, 0.0)
           EIKZ(I, 0) = (1.0, 0.0)

           EIKX(I, 1) = CMPLX ( COS ( TWOPI * RX(I) ), SIN ( TWOPI * RX(I) ) )
           EIKY(I, 1) = CMPLX ( COS ( TWOPI * RY(I) ), SIN ( TWOPI * RY(I) ) )
           EIKZ(I, 1) = CMPLX ( COS ( TWOPI * RZ(I) / LRZ ), SIN ( TWOPI * RZ(I) / LRZ ) )

           EIKY(I, -1) = CONJG ( EIKY(I, 1) )
           EIKZ(I, -1) = CONJG ( EIKZ(I, 1) )

10      CONTINUE

!     ** CALCULATE REMAINING KX, KY AND KZ BY RECURRENCE **

        DO 12 KX = 2, KMAX

           DO 11 I = 1, N

              EIKX(I, KX) = EIKX(I, KX-1) * EIKX(I, 1)

11         CONTINUE

12      CONTINUE

        DO 14 KY = 2, KMAX

           DO 13 I = 1, N

              EIKY(I,  KY) = EIKY(I, KY-1) * EIKY(I, 1)
              EIKY(I, -KY) = CONJG ( EIKY(I, KY) )

13         CONTINUE

14      CONTINUE

        DO 16 KZ = 2, KMAX
              
         
           DO 15 I = 1, N

              EIKZ(I,  KZ) = EIKZ(I, KZ-1) * EIKZ(I, 1)
              EIKZ(I, -KZ) = CONJG ( EIKZ(I, KZ) )

15         CONTINUE

16      CONTINUE

!     ** SUM OVER ALL VECTORS **

        VD   = 0.0
        TOTK = 0

        DO 24 KX = 0, KMAX

           IF ( KX .EQ. 0 ) THEN

              FACTOR = 1.0

           ELSE

              FACTOR = 2.0

           ENDIF

           DO 23 KY = -KMAX, KMAX

              DO 22 KZ = -KMAX, KMAX


                 KSQ = KX * KX + KY * KY + KZ * KZ/LRZ/LRZ

                 IF ( ( KSQ .LT. KSQMAX ) .AND. ( KSQ .NE. 0 ) ) THEN

                    TOTK = TOTK + 1
                    SUM  = (0.0, 0.0)

                    DO 21 I = 1, N

                       EIKR(I) = EIKX(I, KX) * EIKY(I, KY) * EIKZ(I, KZ)
                       SUM     = SUM + Z(I) * EIKR(I)

21                  CONTINUE

                    VD = VD + FACTOR * KVEC(TOTK) * CONJG ( SUM ) * SUM
!                    WRITE(*,*) VD, FACTOR, KVEC(TOTK), CONJG(SUM)
                 ENDIF

22            CONTINUE

23         CONTINUE

24      CONTINUE

!     ** CALCULATES SELF PART OF K-SPACE SUM **

        VS = 0.0

        DO 25 I = 1, N

           VS = VS + Z(I) * Z(I)
      
25      CONTINUE

        VS = RSQPI * KAPPA * VS
!     ** CALCULATE THE TOTAL K-SPACE POTENTIAL **

        VK = VD - VS
        RETURN
        END



        REAL*8 FUNCTION ERFC ( X )

!     *******************************************************************
!     ** APPROXIMATION TO THE COMPLEMENTARY ERROR FUNCTION             **
!     **                                                               **
!     ** REFERENCE:                                                    **
!     **                                                               **
!     ** ABRAMOWITZ AND STEGUN, HANDBOOK OF MATHEMATICAL FUNCTIONS,    **
!     **    NATIONAL BUREAU OF STANDARDS, FORMULA 7.1.26               **
!     *******************************************************************

        REAL*8        A1, A2, A3, A4, A5, P

        PARAMETER ( A1 = 0.254829592, A2 = -0.284496736 )
        PARAMETER ( A3 = 1.421413741, A4 = -1.453152027 )
        PARAMETER ( A5 = 1.061405429, P  =  0.3275911   )

        REAL*8        T, X, XSQ, TP

!     *******************************************************************

        T  = 1.0 / ( 1.0 + P * X )
        XSQ = X * X

        TP = T * ( A1 + T * ( A2 + T * ( A3 + T * ( A4 + T * A5 ) ) ) )

        ERFC = TP * EXP ( -XSQ )

        RETURN
        END

