#!/usr/bin/python
from numpy import *
from pylab import *

infile = open('setdir.inp', "r")
f13fn=zeros((7,6))
Lmax = 0.030 #In Amstrongs
nsteps=8

k=0
for line in infile:
    ls =line.split()
    if len(ls)==8:
        x1, y1, z1, x2, y2, z2, bla, bla = ls
        x1=float(x1); y1=float(y1); z1=float(z1)
        x2=float(x2); y2=float(y2); z2=float(z2)
        f13fn[k] = [x1, y1, z1, x2, y2, z2]
        k = k + 1

for T in (5, 300, 603, 903):
    if (T == 5):
        c = 7.8230; a = 5.8317; 
    elif (T == 300):
        c = 7.8491; a = 5.8598; 
    elif (T == 603):
        c = 7.9004; a = 5.9034; 
    elif (T == 903):
        c = 7.9558; a = 5.9501; 
    l=0    
    tt = str(T)
    for l in range(k):
        if (l==0):
            label = 'dir_f13x+y_' + tt +  '_.txt'
        elif (l==1):
            label = 'dir_f13x-y_' + tt +  '_.txt'
        elif (l==2):
            label = 'dir_f13z_' + tt +  '_.txt'
        elif (l==3):
            label = 'dir_f13x+yf15x+y_' + tt +  '_.txt'
        elif (l==4):
            label = 'dir_f13x-yf15x-y_' + tt +  '_.txt'
        elif (l==5):
            label = 'dir_f13x+yf15x-y_' + tt +  '_.txt'
        elif (l==6):
            label = 'dir_f13zf15z_' + tt +  '_.txt'
        outfile = open (label, "w")
        x1mov=f13fn[l][0]; y1mov=f13fn[l][1]; z1mov=f13fn[l][2]
        x2mov=f13fn[l][3]; y2mov=f13fn[l][4]; z2mov=f13fn[l][5]
    
        if (abs(z1mov) > 0 or abs(z2mov) > 0):
            sizestepc = Lmax/c/nsteps
        else:sizestepc = 0.0
     
        if (abs(x1mov) > 0 or abs(x2mov) > 0 or abs(y1mov) > 0 or abs(y2mov) > 0):    
            sizestepa = Lmax/a/nsteps
        else:sizestepa = 0.0
        stepsize = sqrt(sizestepa*sizestepa + sizestepc*sizestepc)
        nsw=  nsteps+1                       # Number of steps
        potim=stepsize
        delt1=sqrt(x1mov*x1mov + y1mov*y1mov + z1mov*z1mov)
        delt2=sqrt(x2mov*x2mov+y2mov*y2mov+z2mov*z2mov)
        if (delt1 > 0.00000001):
            dir13=array([x1mov, y1mov, z1mov])/delt1
            norm1=sqrt(dir13[0]*dir13[0] + dir13[1]*dir13[1] +  dir13[2]*dir13[2])
        else: dir13=array([0, 0, 0])
        if (delt2 > 0.00000001):
            dirn=array([x2mov, y2mov, z2mov])/delt2
            norm2=sqrt(dirn[0]*dirn[0] + dirn[1]*dirn[1] +  dirn[2]*dirn[2])
        else: dirn=array([0, 0, 0])
        for step in range(1,nsw):
            step=potim*(nsw-step)
            delta13=-step*dir13
            if (delt2 > 0.0000000):
                deltan=-step*dirn
            else: deltan = array([0, 0, 0]); dist = sqrt(delta13[0]*delta13[0] + delta13[1]*delta13[1] + delta13[2]*delta13[2])
            print >> outfile,  delta13[0], delta13[1], delta13[2], deltan[0], deltan[1], deltan[2], -step 
        for step in range(0,nsw):
            step=potim*step
            delta13=step*dir13
            if (delt2 > 0.0000000):
                deltan=step*dirn
            else: deltan = array([0, 0, 0]); dist = sqrt(delta13[0]*delta13[0] + delta13[1]*delta13[1] + delta13[2]*delta13[2])
            print >> outfile,  delta13[0], delta13[1], delta13[2], deltan[0], deltan[1], deltan[2], step
        outfile.close()

infile.close()
